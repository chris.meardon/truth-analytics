import csv
import re
import string

def test_count_words():
    count_words(limit = 1000)

def count_words(**kwargs):
    """
    Given a CSV file with text entries in the fourth column, 
    this function counts the frequency of words in this column
    and stores them in a CSV file. 
    """
    limit = kwargs.get('limit', 0)

    filename = "truths.csv"
    print("Creating list of the words in {}".format(filename))
    words = []
    with open(filename, "r", encoding="utf8") as csvfile:
        reader = csv.reader(csvfile, delimiter = ",")
        next(reader)

        count = 0
        for row in reader:
            if count % 1000 == 0:
                print(count)
            if limit != 0 and count > limit:
                break
            csv_words = row[3].split("\t")
            for i in csv_words:
                words_list = re.findall(r"[\w']+|[.,!?;:\n]",i)
                for word in words_list:
                    if word not in "[.,!?;:\n]":
                        words.append(str.lower(word))
            count += 1

    print("\nTotal text submissions processed: {}". format(count))
    total_words = len(words)
    print("Length of words list: {}".format(total_words))

    words_counted = []
    count = 0
    print("\nStarting word counts...")
    for word in words:
        try:
            if count % 5000 == 0:
                print(count)
            if limit != 0 and count > limit:
                break
            if word not in (i[0] for i in words_counted):
                x = words.count(word)
                words_counted.append((word,x))
            count += 1
        except KeyboardInterrupt:
            print("Interupted. Got to word number {} out of total {} words.".format(count,total_words))
            break

    output_file = "word_counts.csv"
    with open(output_file, "w", encoding = "utf-8", newline = "") as f:
        writer = csv.writer(f)
        writer.writerows(words_counted)
    print("Words counted and saved in {}".format(output_file))